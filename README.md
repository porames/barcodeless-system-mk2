## Barcodeless System Mk2
Barcodeless system is a self-checkout system powered by Machine Learning.
The system is the successor of the original Barcodeless System released back in 2017.
Barcodeless allows users to checkout their items using image recognition and use internet banking (PromptPay) to pay for the items.
This updated version allows the merchant to add new item to the stock on the fly by taking 10 sample images of the item.

## Libraries
Barcodeless System Mk 2 uses Tensorflow JS, a JavaScript library for deep learning on the Internet browser; MobileNet, an image recognition model;
KNN (K-Nearest Neighbors) Classifier which allows merchant to add new data to the machine learning model on the fly.

## Credit
This project is made possible by the support of Mahidol Wittayanusorn School

**Software engineering team**
*  Porames Vatanaprasan
*  Napat Srichan

**Hardware team**
*  Satayu Pasomphong
*  Nattapat Kongteerapat
*  Settasit Munijan

